
First of all, thank you for downloading the starter theme for the runvsc base theme. I have used this base theme for about a year now and have been asked many times to make this available on d.o.

Second, you can find documentation on the major points of runvsc at http://www.runvsc.com

A general heads up:

1. runsvsc is a pre-processing theme. This means that most (well, really all) of the heavy lifting is done in runvsc/template.php and yourvscsubtheme/template.php
2. runvsc's template.php is a collection of functions that are used by the sub themes.
3. you can override most every function in runvsc buy copying it, placing it into your subtheme's folder and renaming it to yoursubthemename where your see runvsc <- this can be done with anyfunction that is referenced in a preprocessing function
4. there ain't any preprocessing functions in runvsc/template.php -> they all live in your subtheme's template.php
5. Download one of the subthemes that I have created (like darkmatter) to get the gist of how this theme engine works <- this will save you hours of pain :)

I have several subthemes that I will be offering up to the community. These sub themes will be hosted on d.o and be referenced at http://www.halfassthemes.com.

Not because these themes are done "half-assed" but, in  my experience, whenever I needed a subtheme, I really found myself stripping out alot what the theme had to make way for what I wanted to do with it. So, I thought, let me just create themes that get the developer about 80% there.

If you don't want "half-ass themes", surf on over to http://www.topnotchthemes.com. I have used "Fusion" (TNT's base theme) for years and it just rocks.

All runvsc themes are designed to have interchangeable classes. ()When you get in there - you will understand what I am talking about.) They play well with Skinr and Block Classes.

I have put alot of love and thought into this theme. Alot of latenight hours - as most of us in open source do. runvsc wasn't created to be like other base themes out there. This project came about because I wanted to create a base theme that was native to how I thought. Alot of the decisions I made might not be "Drupally" - but this project has evolved because people with distinct points of view have given their time and effort to see things through.

If you find a problem or a bug, please make a not of it in the issue queue. I will get to it when I get the chance. If you are majorly stumped - I can be found hanging out in #drupal-themes or at the NYC monthly meetups (http://groups.drupal.org/nyc)

Enjoy!,
johnvsc