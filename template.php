<?php
/**
* Process variables for page.tpl.php
*
* Most themes utilize their own copy of page.tpl.php. The default is located
* inside "modules/system/page.tpl.php". Look in there for the full list of
* variables.
*
* Uses the arg() function to generate a series of page template suggestions
* based on the current path.
*
* Any changes to variables in this preprocessor should also be changed inside
* template_preprocess_maintenance_page() to keep all them consistent.
*
* The $variables array contains the following arguments:
* - $content
* - $show_blocks
*
* @see page.tpl.php
*/
function vsc_whitelight_preprocess_page(& $variables) {
  global $theme ;
  runvsc_build_favicon() ;
  runvsc_build_layout_variables($variables) ;
  $variables['layouts']           = vsc_whitelight_define_layouts() ;
  $variables['head']                = drupal_get_html_head() ;
  $variables['head_title']          = runvsc_build_head_title() ;
  $variables['base_path']           = base_path() ;
  $variables['language']            = $GLOBALS['language'];
  $variables['language']->dir = $GLOBALS['language']->direction ? 'rtl' : 'ltr' ;
  // Track run count for each hook to provide zebra striping.
  $variables['styles']              = drupal_get_css() ;
  $variables['scripts']             = drupal_get_js() ;
  $variables['css']                 = drupal_add_css() ;
  $variables['front_page']          = url() ;
  $variables['breadcrumb']          = runvsc_build_breadcrumb() ;
  $variables['feed_icons']          = runvsc_build_feed_icons() ;
  $variables['footer_message']      = runvsc_build_footer_message() ;
  $variables['help']                = runvsc_build_help() ;
  $variables['logo']                = runvsc_build_logo() ;
  $variables['messages']            = runvsc_build_div_markup("messages", null, $variables['messages'], true);
  $variables['mission']             = runvsc_build_mission() ;
  $variables['primary_links']       = runvsc_build_primary_links($variables['layouts']) ;
  $variables['secondary_links']     = runvsc_build_secondary_links($variables['layouts']) ;
  $variables['search_box']          = runvsc_build_search_box() ;
  $variables['site_name']           = runvsc_build_site_name($variables['layouts']) ;
  $variables['site_slogan']         = runvsc_build_site_slogan() ;
  $variables['tabs']                = runvsc_build_tabs() ;
  $variables['page_title']          = runvsc_build_page_title() ;
  // now that supporting stuff is created: lets markup the content regions!
  $variables['top']                 = runvsc_build_top($variables['top']) ;
  $variables['right']             = vsc_whitelight_build_right($variables['right'], $variables['layouts']) ;
  $variables['left']                = runvsc_build_left($variables['left'], $variables['layouts']) ;
  $variables['header_top']          = runvsc_build_header_top($variables['header_top']) ;
  $variables['header_content']      = runvsc_build_header_content($variables['header_content']) ;
  $variables['preface_top']         = runvsc_build_preface_top($variables['preface_top']) ;
  $variables['preface_bottom']      = runvsc_build_preface_bottom($variables['preface_bottom']) ;
  $variables['content_top']         = runvsc_build_content_top($variables['content_top']) ;
  $variables['content']             = runvsc_build_content($variables['content']) ;
  $variables['content_bottom']      = runvsc_build_content_bottom($variables['content_bottom']) ;
  $variables['postscript_top']      = runvsc_build_postscript_top($variables['postscript_top']) ;
  $variables['postscript_bottom']   = runvsc_build_postscript_bottom($variables['postscript_bottom']) ;
  $variables['footer_content']      = runvsc_build_footer_content($variables['footer_content']) ;
  $variables['bottom']              = runvsc_build_bottom($variables['bottom']) ;
  // Closure should be filled last.
  $variables['closure'] = theme('closure') ;
  if ($node = menu_get_object()) {
    $variables['node'] = $node ;
  }
  // ok, now let's finalize the page:
  runvsc_compile_region_classes($variables) ;
  // we add the "vsc_whitelight" name to the main body class here!
  runvsc_complie_body_classes($variables, 'vsc_whitelight') ;
}

function vsc_whitelight_preprocess_node(& $variables) {
  /* Available variables:
  * - $title: the (sanitized) title of the node.
  * - $content: Node body or teaser depending on $teaser flag.
  * - $picture: The authors picture of the node output from
  *   theme_user_picture().
  * - $date: Formatted creation date (use $created to reformat with
  *   format_date()).
  * - $links: Themed links like "Read more", "Add new comment", etc. output
  *   from theme_links().
  * - $name: Themed username of node author output from theme_username().
  * - $node_url: Direct url of the current node.
  * - $terms: the themed list of taxonomy term links output from theme_links().
  * - $submitted: themed submission information output from
  *   theme_node_submitted().
  *
  * Other variables:
  * - $node: Full node object. Contains data that may not be safe.
  * - $type: Node type, i.e. story, page, blog, etc.
  * - $comment_count: Number of comments attached to the node.
  * - $uid: User ID of the node author.
  * - $created: Time the node was published formatted in Unix timestamp.
  * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
  *   teaser listings.
  * - $id: Position of the node. Increments each time it's output.
  *
  * Node status variables:
  * - $teaser: Flag for the teaser state.
  * - $page: Flag for the full page state.
  * - $promote: Flag for front page promotion state.
  * - $sticky: Flags for sticky post setting.
  * - $status: Flag for published status.
  * - $comment: State of comment settings for the node.
  * - $readmore: Flags true if the teaser content of the node cannot hold the
  *   main body content.
  * - $is_front: Flags true when presented in the front page.
  * - $logged_in: Flags true when the current user is a logged-in member.
  * - $is_admin: Flags true when the current user is an administrator.
  *
  * @see template_preprocess()
  * @see template_preprocess_node()
  */
  $variables['taxonomy']          = runvsc_build_node_taxonomy($variables['node']) ;
  $variables['date']              = format_date($variables['node']->created) ;
  $variables['links']             = runvsc_build_node_links($variables['node']) ;
  $variables['name']              = theme('username', $variables['node']) ;
  $variables['node_url']          = url('node/' . $variables['node']->nid) ;
  $variables['terms']             = runvsc_build_node_terms($variables) ;
  $variables['node_id']           = runvsc_build_node_id($variables['node']) ;
  $variables['node_classes']      = runvsc_build_node_classes($variables) ;
  $variables['title']             = runvsc_build_node_title($variables) ;
  $variables['content']           = runvsc_build_node_content($variables) ;
  // Flatten the node object's member fields.
  $variables = array_merge((array) $variables['node'], $variables) ;
  // Display info only on certain node types.
  $variables['submitted']         = runvsc_build_node_submitted($variables['node']) ;
  $variables['picture']           = runvsc_build_node_picture($variables['node']) ;
  // Clean up name so there are no underscores.
  $variables['template_files'][] = 'node-' . $variables['node']->type ;
}

/**
* Process variables for block.tpl.php
*
* Prepare the values passed to the theme_block function to be passed
* into a pluggable template engine. Uses block properties to generate a
* series of template file suggestions. If none are found, the default
* block.tpl.php is used.
*
* Most themes utilize their own copy of block.tpl.php. The default is located
* inside "modules/system/block.tpl.php". Look in there for the full list of
* variables.
*
* The $variables array contains the following arguments:
* - $block
*
* @see block.tpl.php
*/
function vsc_whitelight_preprocess_block(& $variables) {
  static $block_counter = array() ;
  // All blocks get an independent counter for each region.
  if (!isset ($block_counter[$variables['block']->region])) {
    $block_counter[$variables['block']->region] = 1 ;
  }
  // Same with zebra striping.
  $variables['block_zebra']          = ($block_counter[$variables['block']->region] % 2) ? 'odd' : 'even' ;
  $variables['block_id']             = $block_counter[$variables['block']->region]++;
  $variables['template_files'][]     = 'block-' . $variables['block']->region ;
  $variables['template_files'][]     = 'block-' . $variables['block']->module ;
  $variables['template_files'][]     = 'block-' . $variables['block']->module . '-' . $variables['block']->delta ;
  $variables['block_title']          = runvsc_build_block_title($variables['block']->subject) ;
  $variables['block_content']        = runvsc_build_block_content($variables['block']->content) ;
  $variables['id']                   = runvsc_build_block_id($variables) ;
  $variables['classes']              = runvsc_build_block_classes($variables) ;
  //add admin block links
  runvsc_build_admin_block_links(&$variables);
}

/**
* Process variables for comment.tpl.php.
*
* @see comment.tpl.php
* @see theme_comment()
*/
function vsc_whitelight_preprocess_comment(& $variables) {
  $comment = $variables['comment'];
  $node = $variables['node'];
  $variables['author']              = theme('username', $comment) ;
  $variables['content']             = runvsc_build_comment_content($comment->comment) ;
  $variables['date']                = format_date($comment->timestamp) ;
  $variables['links']               = runvsc_build_comment_links($variables['links']) ;
  $variables['new']                 = runvsc_build_comment_new($comment->new) ;
  $variables['picture']             = theme_get_setting('toggle_comment_user_picture') ? theme('user_picture', $comment) : '' ;
  $variables['signature']           = runvsc_build_comment_signature($comment->signature) ;
  $variables['submitted']           = runvsc_build_comment_submitted($comment) ;
  $variables['title']               = runvsc_build_comment_title(l($comment->subject, $_GET['q'], array('fragment' => "comment-$comment->cid"))) ;
  $variables['template_files'][] = 'comment-' . $node->type ;
  // set status to a string representation of comment->status.
  if (isset ($comment->preview)) {
    $variables['status'] = 'comment-preview' ;
  }
  else {
    $variables['status'] = ($comment->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : 'comment-published' ;
  }
}

/**
* Views preprocessing
* Add view type class (e.g., node, teaser, list, table)
*/
function vsc_whitelight_preprocess_views_view(& $vars) {
  $vars['css_name'] = $vars['css_name'] . ' view-style-' . views_css_safe(strtolower($vars['view']->type)) ;
}

/**
* Theme a single comment block.
*
* @param $comment
*   The comment object.
* @param $node
*   The comment node.
* @param $links
*   An associative array containing control links.
* @param $visible
*   Switches between folded/unfolded view.
* @ingroup themeable
*/
function vsc_whitelight_comment_view($comment, $node, $links = array(), $visible = true) {
  static $first_new = true ;
  $output = '' ;
  $comment->new = node_mark($comment->nid, $comment->timestamp) ;
  if ($first_new && $comment->new != MARK_READ) {
    // Assign the anchor only for the first new comment. This avoids duplicate
    // id attributes on a page.
    $first_new = false ;
    $output .= '<a id="new"></a> <div id="cid-' . $comment->cid . '" class="comment-block new">' ;
  }
  else {
    $output .= '<div id="cid-' . $comment->cid . '" class="comment-block">' ;
  }
  // Switch to folded/unfolded view of the comment
  if ($visible) {
    $comment->comment = check_markup($comment->comment, $comment->format, false) ;
    // Comment API hook
    comment_invoke_comment($comment, 'view') ;
    $output .= theme('comment', $comment, $node, $links) ;
  }
  else {
    $output .= theme('comment_folded', $comment) ;
  }
  $output .= '</div>' ;
  return $output ;
}

/**
* end comment.tpl functions
*/

/*
* vsc_whitelight_define_layouts() is a simple way that you can define 960 grid layouts
* To adjust the grid system, you can change the first value from 16 col to 12 col
* The main areas follow suit, adjusting automatically
* If you remove a left of a right column, the main container adjusts automatically
* You can change these values on a per page basis by overriding this value in a preprocess function :)
*/
function vsc_whitelight_define_layouts() {
  $container = 16 ;
  /*$container = 12 */
  $layout_array = array() ;
  $layout_array["page"]           = array('other' => 'full-width',) ;
  $layout_array["right"]          = array('container' => $container, 'grid' => 4, 'other' => 'omega',) ;
  $layout_array["left"]           = array('container' => $container, 'grid' => 3, 'other' => 'alpha',) ;
  $layout_array["primary"]        = array('container' => $container, 'grid' => 6, 'grid' => $container, 'other' => 'alpha omega',) ;
  $layout_array["secondary"]      = array('container' => $container, 'grid' => 6, 'other' => '',) ;
  $layout_array["site_name"]      = array('container' => $container, 'grid' => 4, 'other' => '',) ;
  $layout_array["logo"]           = array('container' => $container, 'grid' => 4, 'other' => 'alpha',) ;
  $layout_array["default"]        = array('container' => $container, 'other' => '',) ;
  return $layout_array ;
}

/*
* this is a sample of how to use a runvwsc function in a subtheme
* we pinched this function below from runvsc/template.php:
* function runvsc_build_right($region_content, $layouts)
* you can see this function being called in vsc_whitelight_preprocess_page
*/
function vsc_whitelight_build_right($region_content, $layouts) {
  $classes[] = runvsc_build_layout_classes($layouts, 'right') ;
  $classes[] = "together" ;
  if ($region_content) {
    $output = runvsc_build_div_markup("right", $classes, $region_content) ;
    return $output ;
  }
}

/**
* Maintenance page function
* You should always make a maintenance page in case you need to take your site offline!
*/
function vsc_whitelight_preprocess_maintenance_page(& $variables) {
  global $theme ;
  runvsc_build_favicon() ;
  runvsc_build_layout_variables($variables) ;
  $variables['layouts']          = vsc_whitelight_define_layouts() ;
  $variables['head']               = drupal_get_html_head() ;
  $variables['head_title']         = runvsc_build_head_title() ;
  $variables['base_path']          = base_path() ;
  $variables['language']           = $GLOBALS['language'];
  $variables['language']->dir      = $GLOBALS['language']->direction ? 'rtl' : 'ltr' ;
  // Track run count for each hook to provide zebra striping.
  $variables['styles']             = drupal_get_css() ;
  $variables['scripts']            = drupal_get_js() ;
  $variables['css']                = drupal_add_css() ;
  $variables['front_page']         = url() ;
  $variables['page_title']         = runvsc_build_page_title() ;
  // now that supporting stuff is created: lets markup the content regions!
  $variables['content']          = vsc_whitelight_return_maintenance_page_markup() ;
  // Closure should be filled last.
  $variables['closure']           = theme('closure') ;
  if ($node = menu_get_object()) {
    $variables['node'] = $node ;
  }
  // Dead databases will show error messages so supplying this template will
  // allow themers to override the page and the content completely.
  if (isset ($variables['db_is_active']) && !$variables['db_is_active']) {
    $variables['template_file'] = 'maintenance-page-offline' ;
  }
  if (isset ($variables['db_is_active']) && !$variables['db_is_active']) {
    $body_classes[] = 'db-offline' ;
  }
  // ok, now let's finalize the page:
  runvsc_compile_region_classes($variables) ;
  // we add the "vsc_whitelight" name to the main body class here!
  runvsc_complie_body_classes($variables, 'vsc_whitelight') ;
}

function vsc_whitelight_return_maintenance_page_markup() {
  // remember that you need to update your settings.php in order to use the maintenance pages in drupal 6
  // grab site name from the variables table
  $pagetitle            = runvsc_build_tag_markup("h1", "page-title", "title", variable_get('site_name', '') . t(' is being updated.')) ;
  // grab offine message from the variables table
  $message              = runvsc_build_tag_markup("div","message", "offline-content", variable_get('site_offline_message', '')) ;
  $classes              = array('true-block', 'containter-16', 'grid-14', 'alpha', 'omega', 'push-1') ;
  $pagebody             = runvsc_build_div_markup("maintenance-page", $classes, $pagetitle . $message, true) ;
  return $pagebody ;
}